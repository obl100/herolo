from peewee import SqliteDatabase, Database

from app.config import conf


class DBFactory:
    _databases = {
        'sqlite': SqliteDatabase(conf.herolo_db_name)
    }

    @staticmethod
    def create(db_name) -> Database:
        selected_db = DBFactory._databases.get(db_name)
        if not selected_db:
            raise KeyError("Database was not found")

        return selected_db


db: Database = DBFactory.create(conf.herolo_db_type)


def initialize():
    from app.database.models import Message, User, Sender, Receiver

    db.drop_tables([Message, User, Sender, Receiver])
    db.create_tables([Message, User, Sender, Receiver])
