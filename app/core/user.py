import abc

from app.database import models
from app.entities import dto


class UserService(abc.ABC):
    @abc.abstractmethod
    def create_new(self, user_id: dto.User) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    def is_authorized(self, user_credentials: dto.User):
        raise NotImplementedError


# Usually I'd inject the db connection here (so it will be testable).
# But the library is pretty generic and you can use in-memory db for testing
# However, in case I'd have to use some NoSQL db, I'd implement a new User class with other ORM
class UserSQLService(UserService):
    def create_new(self, user: dto.User) -> str:
        if models.User.get_or_none(models.User.username == user.username):
            raise KeyError("Username is taken")

        new_user = models.User(**user.to_dict())
        new_user.hash_password()
        new_user.save(force_insert=True)

        return new_user.id

    def is_authorized(self, user_credentials: dto.User) -> bool:
        user: models.User = models.User.get_or_none(models.User.id == user_credentials.id)

        return user.check_password(user_credentials.password)
