import datetime
import os
from typing import Dict


class BaseConfig:
    JWT_SECRET_KEY = r'\xd1\xe7\xd3A8\x98%\xfb\x85\td\x12\xd2\x0f\t%~7-@\xda\xb0;\x16'
    TOKEN_TTL = datetime.timedelta(days=7)
    herolo_db_type = 'sqlite'
    herolo_db_name = 'herolo.db'


class DevConfig(BaseConfig):
    pass


# For tests
class TestConfig(BaseConfig):
    herolo_db_name = ':memory:'


_configs: Dict[str, type(BaseConfig)] = {
    'development': DevConfig
}


def _get_config(env_name='development') -> BaseConfig:
    env = os.environ.get('herolo_env', env_name)

    env_config = _configs.get(env)
    if not env_config:
        raise EnvironmentError('Could not find requested environment')

    return env_config


conf = _get_config()
