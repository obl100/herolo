from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_restful import Api

from app.config import conf
from app.core.message import MessageSQLService
from app.core.user import UserSQLService
from app.database.setup import db
from app.entities import errors

herolo_app = Flask(__name__)
herolo_app.config.from_object(conf)

herolo_api = Api(herolo_app)
bcrypt = Bcrypt(herolo_app)
jwt = JWTManager(herolo_app)


@herolo_app.before_request
def open_db_connection():
    db.connect()


@herolo_app.errorhandler
def handle_api_error(error: errors.ApiError):
    response = {
        'error': {
            'type': error.__class__.__name__,
            'message': error.message
        }
    }

    return response, error.status_code


@herolo_app.errorhandler
def handle_unexpected_error(error: Exception):
    response = {
        'error': {
            'type': 'UnexpectedException',
            'message': 'An unexpected error has occurred.'
        }
    }

    return response, 500


@herolo_app.teardown_request
def close_db_connection(err):
    db.close()


from app.api.resources.auth import SignIn, SignUp
from app.api.resources.message import Messages, Message

herolo_api.add_resource(Messages, '/api/v1/messages', resource_class_kwargs={'message_service': MessageSQLService()})
herolo_api.add_resource(Message, '/api/v1/messages/<message_id>', resource_class_kwargs={'message_service': MessageSQLService()})

herolo_api.add_resource(SignUp, '/api/v1/auth/signup', resource_class_kwargs={'user_service': UserSQLService()})
herolo_api.add_resource(SignIn, '/api/v1/auth/signin', resource_class_kwargs={'user_service': UserSQLService()})
