import app.database.setup
from app.api.setup import herolo_app

if __name__ == '__main__':
    app.database.setup.initialize()
    herolo_app.run(debug=False)
